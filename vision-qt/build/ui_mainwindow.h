/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAll;
    QAction *actionShowDiff;
    QAction *actionShowWeight;
    QAction *actionShowMap;
    QAction *actionShowLayerInfo;
    QAction *actionShowDeconv;
    QAction *actionShowInput;
    QAction *actionshowlayerLoss;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QGraphicsView *gv_input;
    QGraphicsView *gv_map;
    QListView *lv_layers;
    QVBoxLayout *verticalLayout_3;
    QGraphicsView *gv_deconv;
    QTableWidget *tw_weight;
    QTableWidget *tw_map;
    QMenuBar *menuBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1031, 608);
        MainWindow->setMinimumSize(QSize(1031, 608));
        actionAll = new QAction(MainWindow);
        actionAll->setObjectName(QStringLiteral("actionAll"));
        actionShowDiff = new QAction(MainWindow);
        actionShowDiff->setObjectName(QStringLiteral("actionShowDiff"));
        actionShowWeight = new QAction(MainWindow);
        actionShowWeight->setObjectName(QStringLiteral("actionShowWeight"));
        actionShowMap = new QAction(MainWindow);
        actionShowMap->setObjectName(QStringLiteral("actionShowMap"));
        actionShowLayerInfo = new QAction(MainWindow);
        actionShowLayerInfo->setObjectName(QStringLiteral("actionShowLayerInfo"));
        actionShowDeconv = new QAction(MainWindow);
        actionShowDeconv->setObjectName(QStringLiteral("actionShowDeconv"));
        actionShowInput = new QAction(MainWindow);
        actionShowInput->setObjectName(QStringLiteral("actionShowInput"));
        actionshowlayerLoss = new QAction(MainWindow);
        actionshowlayerLoss->setObjectName(QStringLiteral("actionshowlayerLoss"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_2 = new QHBoxLayout(centralWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gv_input = new QGraphicsView(centralWidget);
        gv_input->setObjectName(QStringLiteral("gv_input"));

        verticalLayout->addWidget(gv_input);

        gv_map = new QGraphicsView(centralWidget);
        gv_map->setObjectName(QStringLiteral("gv_map"));

        verticalLayout->addWidget(gv_map);


        verticalLayout_2->addLayout(verticalLayout);

        lv_layers = new QListView(centralWidget);
        lv_layers->setObjectName(QStringLiteral("lv_layers"));

        verticalLayout_2->addWidget(lv_layers);

        verticalLayout_2->setStretch(0, 1);
        verticalLayout_2->setStretch(1, 1);

        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        gv_deconv = new QGraphicsView(centralWidget);
        gv_deconv->setObjectName(QStringLiteral("gv_deconv"));

        verticalLayout_3->addWidget(gv_deconv);

        tw_weight = new QTableWidget(centralWidget);
        tw_weight->setObjectName(QStringLiteral("tw_weight"));

        verticalLayout_3->addWidget(tw_weight);


        horizontalLayout->addLayout(verticalLayout_3);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 2);

        horizontalLayout_2->addLayout(horizontalLayout);

        tw_map = new QTableWidget(centralWidget);
        tw_map->setObjectName(QStringLiteral("tw_map"));

        horizontalLayout_2->addWidget(tw_map);

        horizontalLayout_2->setStretch(0, 2);
        horizontalLayout_2->setStretch(1, 3);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1031, 22));
        MainWindow->setMenuBar(menuBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        toolBar->setMovable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        toolBar->addAction(actionAll);
        toolBar->addAction(actionShowInput);
        toolBar->addAction(actionShowLayerInfo);
        toolBar->addAction(actionShowMap);
        toolBar->addAction(actionShowWeight);
        toolBar->addAction(actionShowDiff);
        toolBar->addAction(actionShowDeconv);
        toolBar->addAction(actionshowlayerLoss);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Vision", Q_NULLPTR));
        actionAll->setText(QApplication::translate("MainWindow", "GetAll", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionAll->setToolTip(QApplication::translate("MainWindow", "GetAllInformation", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionShowDiff->setText(QApplication::translate("MainWindow", "ShowDiff", Q_NULLPTR));
        actionShowWeight->setText(QApplication::translate("MainWindow", "ShowWeight", Q_NULLPTR));
        actionShowMap->setText(QApplication::translate("MainWindow", "ShowMap", Q_NULLPTR));
        actionShowLayerInfo->setText(QApplication::translate("MainWindow", "ShowLayerInfo", Q_NULLPTR));
        actionShowDeconv->setText(QApplication::translate("MainWindow", "ShowDeconv", Q_NULLPTR));
        actionShowInput->setText(QApplication::translate("MainWindow", "ShowInput", Q_NULLPTR));
        actionshowlayerLoss->setText(QApplication::translate("MainWindow", "showlayerLoss", Q_NULLPTR));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
