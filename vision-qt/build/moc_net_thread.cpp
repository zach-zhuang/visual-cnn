/****************************************************************************
** Meta object code from reading C++ file 'net_thread.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../net_thread.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'net_thread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SendThread_t {
    QByteArrayData data[11];
    char stringdata0[126];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SendThread_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SendThread_t qt_meta_stringdata_SendThread = {
    {
QT_MOC_LITERAL(0, 0, 10), // "SendThread"
QT_MOC_LITERAL(1, 11, 14), // "requestFeature"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 7), // "i_layer"
QT_MOC_LITERAL(4, 35, 7), // "i_image"
QT_MOC_LITERAL(5, 43, 6), // "isDiff"
QT_MOC_LITERAL(6, 50, 16), // "requestLayerInfo"
QT_MOC_LITERAL(7, 67, 13), // "requestWeight"
QT_MOC_LITERAL(8, 81, 12), // "requestInput"
QT_MOC_LITERAL(9, 94, 13), // "requestOutput"
QT_MOC_LITERAL(10, 108, 17) // "requestWeightDiff"

    },
    "SendThread\0requestFeature\0\0i_layer\0"
    "i_image\0isDiff\0requestLayerInfo\0"
    "requestWeight\0requestInput\0requestOutput\0"
    "requestWeightDiff"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SendThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    3,   44,    2, 0x08 /* Private */,
       6,    1,   51,    2, 0x08 /* Private */,
       7,    1,   54,    2, 0x08 /* Private */,
       8,    1,   57,    2, 0x08 /* Private */,
       9,    2,   60,    2, 0x08 /* Private */,
      10,    0,   65,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Bool,    3,    4,    5,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void,

       0        // eod
};

void SendThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SendThread *_t = static_cast<SendThread *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->requestFeature((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 1: _t->requestLayerInfo((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->requestWeight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->requestInput((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->requestOutput((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->requestWeightDiff(); break;
        default: ;
        }
    }
}

const QMetaObject SendThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_SendThread.data,
      qt_meta_data_SendThread,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *SendThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SendThread::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_SendThread.stringdata0))
        return static_cast<void*>(const_cast< SendThread*>(this));
    return QThread::qt_metacast(_clname);
}

int SendThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
struct qt_meta_stringdata_NetThread_t {
    QByteArrayData data[10];
    char stringdata0[90];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NetThread_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NetThread_t qt_meta_stringdata_NetThread = {
    {
QT_MOC_LITERAL(0, 0, 9), // "NetThread"
QT_MOC_LITERAL(1, 10, 12), // "recWeightMap"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 7), // "ITCMsg*"
QT_MOC_LITERAL(4, 32, 12), // "recLayerInfo"
QT_MOC_LITERAL(5, 45, 4), // "data"
QT_MOC_LITERAL(6, 50, 9), // "recDeconv"
QT_MOC_LITERAL(7, 60, 8), // "recInput"
QT_MOC_LITERAL(8, 69, 9), // "recOutput"
QT_MOC_LITERAL(9, 79, 10) // "recFeature"

    },
    "NetThread\0recWeightMap\0\0ITCMsg*\0"
    "recLayerInfo\0data\0recDeconv\0recInput\0"
    "recOutput\0recFeature"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NetThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,
       6,    1,   50,    2, 0x06 /* Public */,
       7,    1,   53,    2, 0x06 /* Public */,
       8,    1,   56,    2, 0x06 /* Public */,
       9,    1,   59,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    5,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    5,
    QMetaType::Void, 0x80000000 | 3,    5,

       0        // eod
};

void NetThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        NetThread *_t = static_cast<NetThread *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->recWeightMap((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 1: _t->recLayerInfo((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 2: _t->recDeconv((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 3: _t->recInput((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 4: _t->recOutput((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 5: _t->recFeature((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (NetThread::*_t)(ITCMsg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&NetThread::recWeightMap)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (NetThread::*_t)(ITCMsg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&NetThread::recLayerInfo)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (NetThread::*_t)(ITCMsg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&NetThread::recDeconv)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (NetThread::*_t)(ITCMsg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&NetThread::recInput)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (NetThread::*_t)(ITCMsg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&NetThread::recOutput)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (NetThread::*_t)(ITCMsg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&NetThread::recFeature)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject NetThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_NetThread.data,
      qt_meta_data_NetThread,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *NetThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NetThread::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_NetThread.stringdata0))
        return static_cast<void*>(const_cast< NetThread*>(this));
    return QThread::qt_metacast(_clname);
}

int NetThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void NetThread::recWeightMap(ITCMsg * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void NetThread::recLayerInfo(ITCMsg * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void NetThread::recDeconv(ITCMsg * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void NetThread::recInput(ITCMsg * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void NetThread::recOutput(ITCMsg * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void NetThread::recFeature(ITCMsg * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
