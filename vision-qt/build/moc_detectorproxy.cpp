/****************************************************************************
** Meta object code from reading C++ file 'detectorproxy.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../detectorproxy.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'detectorproxy.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DetectorProxy_t {
    QByteArrayData data[21];
    char stringdata0[186];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DetectorProxy_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DetectorProxy_t qt_meta_stringdata_DetectorProxy = {
    {
QT_MOC_LITERAL(0, 0, 13), // "DetectorProxy"
QT_MOC_LITERAL(1, 14, 12), // "recWeightMap"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 1), // "w"
QT_MOC_LITERAL(4, 30, 1), // "h"
QT_MOC_LITERAL(5, 32, 1), // "c"
QT_MOC_LITERAL(6, 34, 1), // "n"
QT_MOC_LITERAL(7, 36, 14), // "unsigned char*"
QT_MOC_LITERAL(8, 51, 12), // "recLayerInfo"
QT_MOC_LITERAL(9, 64, 7), // "ITCMsg*"
QT_MOC_LITERAL(10, 72, 4), // "data"
QT_MOC_LITERAL(11, 77, 9), // "recDeconv"
QT_MOC_LITERAL(12, 87, 8), // "recInput"
QT_MOC_LITERAL(13, 96, 9), // "recOutput"
QT_MOC_LITERAL(14, 106, 10), // "recFeature"
QT_MOC_LITERAL(15, 117, 13), // "getFeatureMap"
QT_MOC_LITERAL(16, 131, 9), // "getDeconv"
QT_MOC_LITERAL(17, 141, 8), // "getInput"
QT_MOC_LITERAL(18, 150, 12), // "getLayerInfo"
QT_MOC_LITERAL(19, 163, 9), // "getOutput"
QT_MOC_LITERAL(20, 173, 12) // "getWeightMap"

    },
    "DetectorProxy\0recWeightMap\0\0w\0h\0c\0n\0"
    "unsigned char*\0recLayerInfo\0ITCMsg*\0"
    "data\0recDeconv\0recInput\0recOutput\0"
    "recFeature\0getFeatureMap\0getDeconv\0"
    "getInput\0getLayerInfo\0getOutput\0"
    "getWeightMap"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DetectorProxy[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    5,   74,    2, 0x06 /* Public */,
       8,    1,   85,    2, 0x06 /* Public */,
      11,    4,   88,    2, 0x06 /* Public */,
      12,    4,   97,    2, 0x06 /* Public */,
      13,    1,  106,    2, 0x06 /* Public */,
      14,    5,  109,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      15,    1,  120,    2, 0x08 /* Private */,
      16,    1,  123,    2, 0x08 /* Private */,
      17,    1,  126,    2, 0x08 /* Private */,
      18,    1,  129,    2, 0x08 /* Private */,
      19,    1,  132,    2, 0x08 /* Private */,
      20,    1,  135,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 7,    3,    4,    5,    6,    2,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 7,    3,    4,    5,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 7,    3,    4,    5,    2,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 7,    3,    4,    5,    6,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 9,    2,
    QMetaType::Void, 0x80000000 | 9,    2,
    QMetaType::Void, 0x80000000 | 9,    2,
    QMetaType::Void, 0x80000000 | 9,    2,
    QMetaType::Void, 0x80000000 | 9,    2,
    QMetaType::Void, 0x80000000 | 9,    2,

       0        // eod
};

void DetectorProxy::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DetectorProxy *_t = static_cast<DetectorProxy *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->recWeightMap((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< unsigned char*(*)>(_a[5]))); break;
        case 1: _t->recLayerInfo((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 2: _t->recDeconv((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< unsigned char*(*)>(_a[4]))); break;
        case 3: _t->recInput((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< unsigned char*(*)>(_a[4]))); break;
        case 4: _t->recOutput((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 5: _t->recFeature((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< unsigned char*(*)>(_a[5]))); break;
        case 6: _t->getFeatureMap((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 7: _t->getDeconv((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 8: _t->getInput((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 9: _t->getLayerInfo((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 10: _t->getOutput((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 11: _t->getWeightMap((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (DetectorProxy::*_t)(int , int , int , int , unsigned char * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DetectorProxy::recWeightMap)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (DetectorProxy::*_t)(ITCMsg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DetectorProxy::recLayerInfo)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (DetectorProxy::*_t)(int , int , int , unsigned char * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DetectorProxy::recDeconv)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (DetectorProxy::*_t)(int , int , int , unsigned char * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DetectorProxy::recInput)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (DetectorProxy::*_t)(ITCMsg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DetectorProxy::recOutput)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (DetectorProxy::*_t)(int , int , int , int , unsigned char * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DetectorProxy::recFeature)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject DetectorProxy::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_DetectorProxy.data,
      qt_meta_data_DetectorProxy,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *DetectorProxy::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DetectorProxy::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_DetectorProxy.stringdata0))
        return static_cast<void*>(const_cast< DetectorProxy*>(this));
    return QThread::qt_metacast(_clname);
}

int DetectorProxy::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void DetectorProxy::recWeightMap(int _t1, int _t2, int _t3, int _t4, unsigned char * _t5)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DetectorProxy::recLayerInfo(ITCMsg * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void DetectorProxy::recDeconv(int _t1, int _t2, int _t3, unsigned char * _t4)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void DetectorProxy::recInput(int _t1, int _t2, int _t3, unsigned char * _t4)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void DetectorProxy::recOutput(ITCMsg * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void DetectorProxy::recFeature(int _t1, int _t2, int _t3, int _t4, unsigned char * _t5)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
