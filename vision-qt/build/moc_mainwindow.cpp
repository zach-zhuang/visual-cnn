/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[35];
    char stringdata0[501];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 14), // "requestFeature"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 2), // "i1"
QT_MOC_LITERAL(4, 30, 2), // "i2"
QT_MOC_LITERAL(5, 33, 6), // "isDiff"
QT_MOC_LITERAL(6, 40, 16), // "requestLayerInfo"
QT_MOC_LITERAL(7, 57, 13), // "requestWeight"
QT_MOC_LITERAL(8, 71, 12), // "requestInput"
QT_MOC_LITERAL(9, 84, 13), // "requestOutput"
QT_MOC_LITERAL(10, 98, 17), // "requestWeightDiff"
QT_MOC_LITERAL(11, 116, 8), // "findView"
QT_MOC_LITERAL(12, 125, 17), // "on_tw_map_clicked"
QT_MOC_LITERAL(13, 143, 5), // "index"
QT_MOC_LITERAL(14, 149, 20), // "on_lv_layers_clicked"
QT_MOC_LITERAL(15, 170, 13), // "getFeatureMap"
QT_MOC_LITERAL(16, 184, 14), // "unsigned char*"
QT_MOC_LITERAL(17, 199, 9), // "getDeconv"
QT_MOC_LITERAL(18, 209, 1), // "w"
QT_MOC_LITERAL(19, 211, 1), // "h"
QT_MOC_LITERAL(20, 213, 1), // "c"
QT_MOC_LITERAL(21, 215, 8), // "getInput"
QT_MOC_LITERAL(22, 224, 12), // "getLayerInfo"
QT_MOC_LITERAL(23, 237, 7), // "ITCMsg*"
QT_MOC_LITERAL(24, 245, 9), // "getOutput"
QT_MOC_LITERAL(25, 255, 12), // "getWeightMap"
QT_MOC_LITERAL(26, 268, 1), // "n"
QT_MOC_LITERAL(27, 270, 20), // "on_tw_weight_clicked"
QT_MOC_LITERAL(28, 291, 27), // "on_actionShowDiff_triggered"
QT_MOC_LITERAL(29, 319, 29), // "on_actionShowWeight_triggered"
QT_MOC_LITERAL(30, 349, 26), // "on_actionShowMap_triggered"
QT_MOC_LITERAL(31, 376, 32), // "on_actionShowLayerInfo_triggered"
QT_MOC_LITERAL(32, 409, 29), // "on_actionShowDeconv_triggered"
QT_MOC_LITERAL(33, 439, 28), // "on_actionShowInput_triggered"
QT_MOC_LITERAL(34, 468, 32) // "on_actionshowlayerLoss_triggered"

    },
    "MainWindow\0requestFeature\0\0i1\0i2\0"
    "isDiff\0requestLayerInfo\0requestWeight\0"
    "requestInput\0requestOutput\0requestWeightDiff\0"
    "findView\0on_tw_map_clicked\0index\0"
    "on_lv_layers_clicked\0getFeatureMap\0"
    "unsigned char*\0getDeconv\0w\0h\0c\0getInput\0"
    "getLayerInfo\0ITCMsg*\0getOutput\0"
    "getWeightMap\0n\0on_tw_weight_clicked\0"
    "on_actionShowDiff_triggered\0"
    "on_actionShowWeight_triggered\0"
    "on_actionShowMap_triggered\0"
    "on_actionShowLayerInfo_triggered\0"
    "on_actionShowDeconv_triggered\0"
    "on_actionShowInput_triggered\0"
    "on_actionshowlayerLoss_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,  129,    2, 0x06 /* Public */,
       6,    1,  136,    2, 0x06 /* Public */,
       7,    1,  139,    2, 0x06 /* Public */,
       8,    1,  142,    2, 0x06 /* Public */,
       9,    2,  145,    2, 0x06 /* Public */,
      10,    0,  150,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,  151,    2, 0x08 /* Private */,
      12,    1,  152,    2, 0x08 /* Private */,
      14,    1,  155,    2, 0x08 /* Private */,
      15,    5,  158,    2, 0x08 /* Private */,
      17,    4,  169,    2, 0x08 /* Private */,
      21,    4,  178,    2, 0x08 /* Private */,
      22,    1,  187,    2, 0x08 /* Private */,
      24,    1,  190,    2, 0x08 /* Private */,
      25,    5,  193,    2, 0x08 /* Private */,
      27,    1,  204,    2, 0x08 /* Private */,
      28,    0,  207,    2, 0x08 /* Private */,
      29,    0,  208,    2, 0x08 /* Private */,
      30,    0,  209,    2, 0x08 /* Private */,
      31,    0,  210,    2, 0x08 /* Private */,
      32,    0,  211,    2, 0x08 /* Private */,
      33,    0,  212,    2, 0x08 /* Private */,
      34,    0,  213,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Bool,    3,    4,    5,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   13,
    QMetaType::Void, QMetaType::QModelIndex,   13,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 16,    2,    2,    2,    2,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 16,   18,   19,   20,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 16,   18,   19,   20,    2,
    QMetaType::Void, 0x80000000 | 23,    2,
    QMetaType::Void, 0x80000000 | 23,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 16,   18,   19,   20,   26,    2,
    QMetaType::Void, QMetaType::QModelIndex,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->requestFeature((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 1: _t->requestLayerInfo((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->requestWeight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->requestInput((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->requestOutput((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->requestWeightDiff(); break;
        case 6: _t->findView(); break;
        case 7: _t->on_tw_map_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 8: _t->on_lv_layers_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 9: _t->getFeatureMap((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< unsigned char*(*)>(_a[5]))); break;
        case 10: _t->getDeconv((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< unsigned char*(*)>(_a[4]))); break;
        case 11: _t->getInput((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< unsigned char*(*)>(_a[4]))); break;
        case 12: _t->getLayerInfo((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 13: _t->getOutput((*reinterpret_cast< ITCMsg*(*)>(_a[1]))); break;
        case 14: _t->getWeightMap((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< unsigned char*(*)>(_a[5]))); break;
        case 15: _t->on_tw_weight_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 16: _t->on_actionShowDiff_triggered(); break;
        case 17: _t->on_actionShowWeight_triggered(); break;
        case 18: _t->on_actionShowMap_triggered(); break;
        case 19: _t->on_actionShowLayerInfo_triggered(); break;
        case 20: _t->on_actionShowDeconv_triggered(); break;
        case 21: _t->on_actionShowInput_triggered(); break;
        case 22: _t->on_actionshowlayerLoss_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)(int , int , bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::requestFeature)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::requestLayerInfo)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::requestWeight)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::requestInput)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::requestOutput)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::requestWeightDiff)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 23;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::requestFeature(int _t1, int _t2, bool _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::requestLayerInfo(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::requestWeight(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MainWindow::requestInput(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MainWindow::requestOutput(int _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MainWindow::requestWeightDiff()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
