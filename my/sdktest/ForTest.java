
import com.zoson.detection.*;
import com.zoson.detection.proto.*;
import cn.edu.scut.iais.proto.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintStream;
import com.google.protobuf.TextFormat;
import java.awt.image.BufferedImage;
import cn.edu.scut.iais.*;
import java.io.File;
import javax.imageio.ImageIO;

public class ForTest
{
	public static void main(String[] args)
	{
		System.loadLibrary("detectorJni");
		test_imageRecognition();
	}

	public static boolean test_imageRecognition()
	{
		ImageRecognition im = new ImageRecognition("/home/zoson/detector/my/config/dcontext.prototxt");//更改dcontext.prototxt路径
		im.init();
		DetectorProto.ObjectDetect res =  im.detectObject("/home/zoson/detector/my/data/test/timg.jpg");//目标检测图片
		System.out.println("object state code ::"+res.getStateCode());
		System.out.println("object count::"+res.getObjectListCount());
		int count = res.getObjectListCount();
		for(int i=0;i<count;++i)
		{
			DetectorProto.Object ob = res.getObjectList(i);
			System.out.println("Object id "+ob.getObjectId());
			System.out.println("Object score "+ob.getScore());
			System.out.println("Object x "+ob.getX());
			System.out.println("Object y "+ob.getY());
			System.out.println("Object w "+ob.getW());
			System.out.println("Object H "+ob.getH());
		}
		DetectorProto.SceneDetect res1 =  im.detectScene("/home/zoson/detector/my/data/test/shui.jpg");//场景检测图片
		System.out.println("object statecode::"+res1.getStateCode());//
		System.out.println("object id::"+res1.getSceneId());//scene id 比如fire为０
		System.out.println("object score::"+res1.getScore());//score为概率
		return false;
	}


}
